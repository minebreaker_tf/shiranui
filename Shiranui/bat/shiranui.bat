@echo off

set JVM_ARGS="-Dosgi.requiredJavaVersion=1.8 -XX:+UseSerialGC"

set CURRENT=%~dp0
cd /d %CURRENT%

set CLASSPATH=".\bin\Shiranui.jar;.\lib\commons-io-2.4.jar;.\lib\gson-2.6.1.jar;.\lib\guava-19.0.jar"

if defined JAVA_HOME (
    set JAVA_EXE="java.exe"
) else (
    set JAVA_EXE="%JAVA_HOME%\bin\java.exe"
)

%JAVA_EXE% %JVM_ARGS% -classpath %CLASSPATH% "shiranui.Main" %*
