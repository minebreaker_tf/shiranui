package shiranui;

import org.junit.Test;

import shiranui.utils.config.Config;
import shiranui.utils.config.ConfigBuilder;

public class ServiceNonInteractiveTest {

    @Test
    public void test() {

        Config conf = new ConfigBuilder().data("dat/data.bin").build();

        Service service = ServiceNonInteractive.getInstance();
        String[] args = {"piyo", "list"};
        service.execute(args, conf);
    }

    @Test
    public void autoLoginTest() {
        Config conf = new ConfigBuilder().data("dat/data.bin").password("piyo").build();

        Service service = ServiceNonInteractive.getInstance();
        String[] args = {"list"};
        service.execute(args, conf);
    }

}
