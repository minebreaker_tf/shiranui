package shiranui.utils.config;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

public class ConfigTest {

    private Reader reader;
    private Gson gson;

    @Before
    public void setUp() throws IOException {
        reader = Files.newBufferedReader(Paths.get("conf/conf.json"));
        gson = new Gson();
    }

    @After
    public void tearDown() throws IOException {
        reader.close();
    }

    // @Ignore
    @Test
    public void test() throws Exception {
        Config conf = gson.fromJson(reader, Config.class);

        assertThat(conf.getAutosave(), is(false));
        assertThat(conf.getPassword(), is(""));
        assertThat(conf.getData(), is("dat/data.bin"));
    }

    @Test
    public void testConf() throws Exception {
        String gsonStr = "{\"autosave\": true, \"password\": \"piyo\", \"data\": \"dat/data.bin\"}";
        Config conf = gson.fromJson(gsonStr, Config.class);

        assertThat(conf.getAutosave(), is(true));
        assertThat(conf.getPassword(), is("piyo"));
        assertThat(conf.getData(), is("dat/data.bin"));
    }

}
