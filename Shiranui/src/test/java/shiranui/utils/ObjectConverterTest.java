package shiranui.utils;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import javax.xml.bind.DatatypeConverter;

import org.junit.Test;

public class ObjectConverterTest {

    @Test
    public void test() {
        String original = "Original Message";
        byte[] serialized = ObjectConverter.serialize(original);
        System.out.println(DatatypeConverter.printHexBinary(serialized));

        String deserialized = (String)ObjectConverter.deserialize(serialized);
        System.out.println(deserialized);

        assertThat(original, is(deserialized));
    }

}
