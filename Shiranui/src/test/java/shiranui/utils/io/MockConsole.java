package shiranui.utils.io;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public final class MockConsole implements IoConsole {

    private final Deque<String> in;
    private final List<String> out;

    public MockConsole(final List<String> in) {
        this.in = new LinkedList<>(in);
        this.out = new ArrayList<>();
    }

    @Override
    public String readLine() {
        return in.pop();
    }

    @Override
    public char[] readPassword() {
        return in.pop().toCharArray();
    }

    @Override
    public void write(final Message message) {
        out.add(message.getSimple());
    }

    public List<String> getOutputs() {
        return Collections.unmodifiableList(out);
    }

}
