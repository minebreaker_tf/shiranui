package shiranui.utils.io;

import org.junit.Ignore;
import org.junit.Test;

public class StandardIoConsoleTest {

    @Ignore //手動用
    @Test
    public void test() {
        IoConsole console = new StandardIoConsole(Message.Level.FULL);

        console.writeLine("hoge");
        String value = console.readLine();
        console.writeLine(value);
    }

}
