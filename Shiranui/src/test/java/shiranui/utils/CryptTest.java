package shiranui.utils;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import javax.xml.bind.DatatypeConverter;

import org.junit.Ignore;
import org.junit.Test;

import shiranui.utils.io.IoConsole;
import shiranui.utils.io.IoConsoleFactory;
import shiranui.utils.io.Message;

public class CryptTest {

    //手動実行用
    @Ignore
    @Test
    public void testInput() throws Exception {
        IoConsole console = IoConsoleFactory.getConsole(Message.Level.FULL);
        System.out.print("Key: ");
        String key = console.readLine();
        Crypt crypt = new Crypt(key);

        System.out.print("Src: ");
        String src = console.readLine();
        byte[] crypted = crypt.encode(src);
        System.out.println("Binary: " + DatatypeConverter.printHexBinary(src.getBytes()));
        System.out.println("Crypted: " + DatatypeConverter.printHexBinary(crypted));

        byte[] decrypted = crypt.decode(crypted);
        String original = new String(decrypted);
        System.out.println("Decrypted: " + original);
        System.out.println("Binary: " + DatatypeConverter.printHexBinary(decrypted));

        assertThat(src, is(original));
        assertThat(src.getBytes(), is(decrypted));
    }

    @Test
    public void testEncodeAndDecode() throws Exception {
        String key = "Your password";
        Crypt crypt = new Crypt(key);

        String src = "This line will be crypted and decrypted.";
        byte[] crypted = crypt.encode(src);

        assertThat(src.getBytes(), not(crypted));

        byte[] decrypted = crypt.decode(crypted);
        String original = new String(decrypted);

        assertEquals(src, original);
        assertArrayEquals(src.getBytes(), decrypted);
    }

}
