package shiranui;

import shiranui.utils.config.Config;

/**
 * Shiranuiの起動後の処理を担うインターフェース。<br />
 * この実装がアプリケーションの"モード"になる。
 */
interface Service {

    /**
     * サービスを起動する。
     * @param args JVM引数
     * @param config アプリケーションの設定
     */
    public abstract void execute(String[] args, Config config);

}
