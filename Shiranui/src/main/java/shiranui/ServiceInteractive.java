package shiranui;

import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;

import shiranui.command.Command;
import shiranui.command.Parameter;
import shiranui.consts.Consts;
import shiranui.utils.Crypt;
import shiranui.utils.Hash;
import shiranui.utils.ServiceUtil;
import shiranui.utils.config.Config;
import shiranui.utils.io.IoConsole;
import shiranui.utils.io.IoConsoleFactory;
import shiranui.utils.io.Message;

/**
 * 対話的実行を提供するサービス。
 */
final class ServiceInteractive implements Service {

    /** シングルトンインスタンス */
    private static final ServiceInteractive singleton = new ServiceInteractive();

    /** インスタンス化禁止 */
    private ServiceInteractive() {}

    /**
     * シングルトンインスタンスを取得する。
     */
    public static ServiceInteractive getInstance() {
        return singleton;
    }

    @Override
    public void execute(String[] programArgs, Config conf) {

        IoConsole console = IoConsoleFactory.getConsole(Message.Level.FULL);
        console.writeLine(Consts.EXPLAIN);

        byte[] contentCrypted = ServiceUtil.readCryptedContent(console, Paths.get(conf.getData()));
        console.writeLine("Data File MD5: " + Hash.getInstance().getHashString(contentCrypted));

        Crypt crypt;
        Map<String, String> dataMap;

        // TODO リファクタリング
        //データファイルが存在するか
        if (conf.getPassword() != null && !conf.getPassword().isEmpty()) { // パスワードが設定されている
            console.writeLine("Note: auto-login is enabled.");
            crypt = new Crypt(conf.getPassword());
            try {
                dataMap = ServiceUtil.convertBytesToMap(console, crypt.decode(contentCrypted));
            } catch (InvalidKeyException | BadPaddingException e) {
                console.writeLine("Saved key is invalid. Please check.");
                throw new Error(); // エラーの種類を変えてもいいかもしれない(あるいは非チェック例外?)
            }
        } else if (contentCrypted.length == 0) { //存在する
            console.write("Please specify your password for new data file: ");
            crypt = new Crypt(console.readPassword());
            dataMap = new HashMap<>();
        } else { //存在しない
            console.write("Please specify your password: ");
            crypt = askPassword(console, contentCrypted);
            try {
                // 2回解読しているので何とかしたいが、シンプルさと両立しない :<
                dataMap = ServiceUtil.convertBytesToMap(console, crypt.decode(contentCrypted));
                // askPassword()で正しいキーであることを確認しているので、例外は起こらないはず
            } catch (InvalidKeyException | BadPaddingException e) {
                e.printStackTrace();
                throw new Error(e);
            }
        }

        Parameter parameter = Parameter.create(
                dataMap,
                console,
                crypt,
                Collections.emptyList(),
                conf);

        runCommands(parameter);

    }

    /**
     * パスワードを、正しいものが指定されるまで繰り返し訊ねる。
     * @param console 対話に利用するコンソール
     * @param contentCrypted 暗号化されたコンテンツ
     * @return 解読に成功したCryptオブジェクト
     */
    private Crypt askPassword(IoConsole console, byte[] contentCrypted) {
        while (true) {
            Crypt testCrypt = new Crypt(console.readPassword());
            try {
                // 入力されたキーで解読を試みる
                testCrypt.decode(contentCrypted);

                //解読に成功
                return testCrypt;
            } catch (InvalidKeyException | BadPaddingException e) {
                //デコードに失敗すると例外が発生
                console.write("Failed to decode. Please try again: ");
            }
        }
    }

    /**
     * ユーザーに繰り返し、命令の入力を求める。
     * @param parameter 最初に実行するパラメーター
     */
    private void runCommands(Parameter parameter) {
        //noinspection InfiniteLoopStatement
        while (true) {
            parameter.getConsole().write("> ");

            List<String> args = Arrays.asList(parameter.getConsole().readLine().split(" "));
            Command command = Command.getCommand(args.get(0));

            parameter.setArgs(args);

            Parameter result = command.execute(parameter);
            parameter = result;
        }
    }

}
