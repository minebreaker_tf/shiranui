package shiranui.command;

import shiranui.command.strategy.*;
import shiranui.utils.io.Message;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * 各コマンドと戦略を結び付ける
 */
public enum Command {

    ADD(AddStrategy.getInstance()),
    REMOVE(RemoveStrategy.getInstance()),
    SHOW(ShowStrategy.getInstance()),
    LIST(ListStrategy.getInstance()),

    GENERATE(GenerateStrategy.getInstance()),
    GEN(GenerateStrategy.getInstance()),
    CHANGE(ChangeStrategy.getInstance()),
    REPLACE(ReplaceStrategy.getInstance()),
    FIND(FindStrategy.getInstance()),
    CONTAINS(FindStrategy.getInstance()),
    SEARCH(SearchStrategy.getInstance()),

    SAVE(SaveStrategy.getInstance()),
    STORE(SaveStrategy.getInstance()),
    COMMIT(SaveStrategy.getInstance()),

    CHANGEPASSWORD(ChangePasswordStrategy.getInstance()),
    CHPASSWD(ChangePasswordStrategy.getInstance()),
    PASSWORD(ChangePasswordStrategy.getInstance()),
    RENEW(ChangePasswordStrategy.getInstance()),

    HELP(HelpStrategy.getInstance()),

    EXIT(ExitStrategy.getInstance()),
    QUIT(ExitStrategy.getInstance()),
    BYE(ExitStrategy.getInstance()),

    ILLEGAL_COMMAND((parameter) -> {
        parameter.getConsole().writeLine(new Message(
                "illegal command",
                "Illegal command: " + parameter.getArgs().get(0)));
        return parameter;
    });

    /**
     * 保持している戦略パターン
     */
    private Strategy strategy;

    /**
     * コンストラクター
     * @param strategy 設定する戦略パターン
     */
    private Command(Strategy strategy) {
        this.strategy = strategy;
    }

    /**
     * 文字列からCommandインスタンスを取得する。
     * @param value コマンドの名前
     * @return Command
     */
    public static Command getCommand(String value) {
        checkArgument(!isNullOrEmpty(value));

        // TODO もっと格好いいやり方を考える
        Command command;
        try {
            command = Command.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            command = Command.ILLEGAL_COMMAND;
        }
        return command;
    }

    /**
     * コマンドを実行する。
     * @param parameter 実行に使用されるパラメーター
     * @return 実行結果パラメーター
     */
    public Parameter execute(Parameter parameter) {
        checkNotNull(parameter);

        // TODO リザルトは別クラスの方がいいかもしれない(本来はそれが正しいし
        return strategy.execute(parameter);
    }

}
