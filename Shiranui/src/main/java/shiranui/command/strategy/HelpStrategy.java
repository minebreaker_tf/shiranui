package shiranui.command.strategy;

import shiranui.command.Parameter;
import shiranui.consts.Consts;
import shiranui.utils.io.IoConsole;
import shiranui.utils.io.Message;

public class HelpStrategy implements Strategy {

    private static final HelpStrategy singleton = new HelpStrategy();

    private HelpStrategy() {}

    public static HelpStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        String sep = System.lineSeparator();
        StringBuilder builder = new StringBuilder()
                .append(Consts.EXPLAIN + sep).append("").append(sep)
                .append("add [key] [password]").append(sep)
                .append("remove [key]").append(sep)
                .append("show [key]").append(sep)
                .append("list").append(sep)
                .append("generate [key]").append(sep)
                .append("save").append(sep)
                .append("help").append(sep)
                .append("exit").append(sep);

        IoConsole console = parameter.getConsole();
        console.writeLine(new Message(builder.toString(), builder.toString()));

        return parameter;
    }

}
