package shiranui.command.strategy;

import shiranui.command.Parameter;

@FunctionalInterface
public interface Strategy {
    public abstract Parameter execute(Parameter parameter);
}
