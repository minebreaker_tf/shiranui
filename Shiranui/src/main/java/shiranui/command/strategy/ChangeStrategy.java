package shiranui.command.strategy;

import java.util.Map;

import shiranui.command.Parameter;
import shiranui.utils.file.SaveUtil;
import shiranui.utils.io.IoConsole;

public class ChangeStrategy implements Strategy {

    private static final ChangeStrategy singleton = new ChangeStrategy();

    private ChangeStrategy() {}

    public static ChangeStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        if (parameter.getArgs().size() != 3) {
            console.writeLine("Illegal args.");
            console.writeLine("(e.g.) change key password");
            return parameter;
        }

        Map<String, String> map = parameter.getPasswordMap();
        String key = parameter.getArgs().get(1);
        String value = parameter.getArgs().get(2);

        if (map.containsKey(key)) {
            map.put(key, value);
            console.writeLine("Changed.");
        } else {
            console.writeLine("Key not found. Create new.");
            map.put(key, value);
        }

        if (parameter.getConfig().getAutosave()) {
            SaveUtil.save(parameter);
        }

        return parameter;
    }

}
