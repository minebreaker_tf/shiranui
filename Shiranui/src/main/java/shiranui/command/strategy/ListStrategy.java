package shiranui.command.strategy;

import java.util.Map;

import shiranui.command.Parameter;
import shiranui.utils.io.IoConsole;
import shiranui.utils.io.Message;

public class ListStrategy implements Strategy {

    private static final ListStrategy singleton = new ListStrategy();

    private ListStrategy() {}

    public static ListStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        Map<String, String> map = parameter.getPasswordMap();
        IoConsole console = parameter.getConsole();

        if (map.isEmpty()) {
            console.writeLine("Empty.");
            return parameter;
        }

        console.writeLine("List:");
        map.forEach((key, value) -> console.writeLine(new Message(key, key)));
        console.writeLine(":end");

        return parameter;
    }

}
