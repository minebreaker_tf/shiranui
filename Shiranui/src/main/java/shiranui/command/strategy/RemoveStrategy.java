package shiranui.command.strategy;

import java.util.Map;

import shiranui.command.Parameter;
import shiranui.utils.file.SaveUtil;
import shiranui.utils.io.IoConsole;

public class RemoveStrategy implements Strategy {
    private static final RemoveStrategy singleton = new RemoveStrategy();

    private RemoveStrategy() {}

    public static RemoveStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        if (parameter.getArgs().size() != 2) {
            console.writeLine("Illegal args.");
            console.writeLine("(e.g.) remove key");
            return parameter;
        }

        String key = parameter.getArgs().get(1);

        Map<String, String> map = parameter.getPasswordMap();
        if (!map.containsKey(key)) {
            console.writeLine("Key not found.");
            return parameter;
        }

        console.write("Are you sure? (Y/N): ");
        if (console.readYN()) {
            map.remove(key);
            console.writeLine("Removed.");
        } else {
            console.writeLine("Removing canceled.");
        }

        if (parameter.getConfig().getAutosave()) {
            SaveUtil.save(parameter);
        }

        return parameter;

    }

}
