package shiranui.command.strategy;

import java.util.Map;

import shiranui.command.Parameter;
import shiranui.utils.file.SaveUtil;
import shiranui.utils.io.IoConsole;

public class ReplaceStrategy implements Strategy {

    private static final ReplaceStrategy singleton = new ReplaceStrategy();

    private ReplaceStrategy() {}

    public static ReplaceStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        if (parameter.getArgs().size() != 3) {
            console.writeLine("Illegal args.");
            console.writeLine("(e.g.) replaced key password");
            return parameter;
        }

        Map<String, String> map = parameter.getPasswordMap();
        String key = parameter.getArgs().get(1);
        String value = parameter.getArgs().get(2);

        if (!map.containsKey(key)) {
            console.writeLine("Key not found.");
            return parameter;
        }

        map.put(key, value);
        console.writeLine("Replaced.");

        if (parameter.getConfig().getAutosave()) {
            SaveUtil.save(parameter);
        }

        return parameter;
    }

}
