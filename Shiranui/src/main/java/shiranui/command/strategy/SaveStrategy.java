package shiranui.command.strategy;

import shiranui.command.Parameter;
import shiranui.utils.file.SaveUtil;
import shiranui.utils.io.IoConsole;

public class SaveStrategy implements Strategy {

    private static final SaveStrategy singleton = new SaveStrategy();

    private SaveStrategy() {}

    public static SaveStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        console.write("Are you sure? (Y/N) : ");
        if (!console.readYN()) {
            console.writeLine("Saving canceled.");
            return parameter;
        } else {

            SaveUtil.save(parameter);

            return parameter;
        }
    }

}
