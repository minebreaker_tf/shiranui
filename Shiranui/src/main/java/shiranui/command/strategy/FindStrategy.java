package shiranui.command.strategy;

import java.util.List;
import java.util.stream.Collectors;

import shiranui.command.Parameter;
import shiranui.utils.io.IoConsole;
import shiranui.utils.io.Message;

public class FindStrategy implements Strategy {

    private static final FindStrategy singleton = new FindStrategy();

    private FindStrategy() {}

    public static FindStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        if (parameter.getArgs().size() != 2) {
            console.writeLine("Illegal args.");
            console.writeLine("(e.g.) find query");
            return parameter;
        }

        List<String> matched = parameter.getPasswordMap().keySet().stream()
                .filter((key) -> key.contains(parameter.getArgs().get(1)))
                .sorted()
                .collect(Collectors.toList());

        if (matched.isEmpty()) {
            console.writeLine("Not found.");
        } else {
            console.writeLine("Find:");
            matched.forEach((key) -> console.writeLine(new Message(key, key)));
            console.writeLine(":end");
        }

        return parameter;
    }

}
