package shiranui.command.strategy;

import shiranui.command.Parameter;
import shiranui.utils.Crypt;
import shiranui.utils.file.SaveUtil;
import shiranui.utils.io.IoConsole;

public class ChangePasswordStrategy implements Strategy {

    private static final ChangePasswordStrategy singleton = new ChangePasswordStrategy();

    private ChangePasswordStrategy() {}

    public static ChangePasswordStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        String newPassword;
        if (parameter.getArgs().size() == 1) {
            console.write("Enter new password (or blank not to change): ");
            newPassword = console.readLine();
            if (newPassword.isEmpty()) {
                return parameter;
            }
        } else if (parameter.getArgs().size() == 2) {
            newPassword = parameter.getArgs().get(1);
        } else {
            console.writeLine("Illegal args.");
            console.writeLine("(e.g.) generate key length");
            return parameter;
        }

        Crypt newCrypt = new Crypt(newPassword);
        console.writeLine("Password has changed. Change will not take effect until be saved.");
        parameter.setCrypt(newCrypt);

        if (parameter.getConfig().getAutosave()) {
            SaveUtil.save(parameter);
        }

        return parameter;
    }

}
