package shiranui.command.strategy;

import shiranui.command.Parameter;
import shiranui.utils.file.SaveUtil;
import shiranui.utils.io.IoConsole;

public class ExitStrategy implements Strategy {

    private static final int EXIT_SUCCESS = 0;

    private static final ExitStrategy singleton = new ExitStrategy();

    private ExitStrategy() {}

    public static ExitStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {
        IoConsole console = parameter.getConsole();

        console.writeLine("Save before exit? (Y/N)");
        if (console.readYN()) {
            SaveUtil.save(parameter);
        }

        console.writeLine("Bye.");
        System.exit(EXIT_SUCCESS);

        //形式上のリターン
        return null;
    }

}
