package shiranui.command.strategy;

import java.util.Map;

import shiranui.command.Parameter;
import shiranui.utils.ClipboardUtil;
import shiranui.utils.io.IoConsole;
import shiranui.utils.io.Message;

public class ShowStrategy implements Strategy {

    private static final ShowStrategy singleton = new ShowStrategy();

    private ShowStrategy() {}

    public static ShowStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        if (parameter.getArgs().size() != 2) {
            console.writeLine("Illegal args.");
            console.writeLine("(e.g.) show key");
            return parameter;
        }

        String key = parameter.getArgs().get(1);

        Map<String, String> map = parameter.getPasswordMap();
        if (!map.containsKey(key)) {
            console.writeLine("Key not found.");
            return parameter;
        }

        String value = map.get(key);
        console.writeLine(new Message(value, "Value: " + value));

        ClipboardUtil.setString(value);

        return parameter;

    }
}
