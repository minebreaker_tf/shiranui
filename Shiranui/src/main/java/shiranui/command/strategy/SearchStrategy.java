package shiranui.command.strategy;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import shiranui.command.Parameter;
import shiranui.utils.io.IoConsole;
import shiranui.utils.io.Message;

public class SearchStrategy implements Strategy {

    private static final SearchStrategy singleton = new SearchStrategy();

    private SearchStrategy() {}

    public static SearchStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        if (parameter.getArgs().size() != 2) {
            console.writeLine("Illegal args.");
            console.writeLine("(e.g.) search query");
            return parameter;
        }

        Pattern pattern = Pattern.compile(parameter.getArgs().get(1));

        List<String> matched = parameter.getPasswordMap().keySet().stream()
                .filter(pattern.asPredicate())
                .sorted()
                .collect(Collectors.toList());

        if (matched.isEmpty()) {
            console.writeLine("Not found.");
        } else {
            console.writeLine("Search:");
            matched.forEach((key) -> console.writeLine(new Message(key, key)));
            console.writeLine(":end");
        }

        return parameter;
    }

}
