package shiranui.command.strategy;

import shiranui.command.Parameter;
import shiranui.utils.chars.CharacterGenerator;
import shiranui.utils.file.SaveUtil;
import shiranui.utils.io.IoConsole;
import shiranui.utils.io.Message;

public class GenerateStrategy implements Strategy {

    private static final GenerateStrategy singleton = new GenerateStrategy();

    private GenerateStrategy() {}

    public static GenerateStrategy getInstance() {
        return singleton;
    }

    @Override
    public Parameter execute(Parameter parameter) {

        IoConsole console = parameter.getConsole();

        if (parameter.getArgs().size() != 3) {
            console.writeLine("Illegal args.");
            console.writeLine("(e.g.) generate key length");
            return parameter;
        }

        int bound;
        try {
            bound = Integer.valueOf(parameter.getArgs().get(2));
        } catch (NumberFormatException e) {
            console.writeLine("Illegal argument: " + parameter.getArgs().get(2) + " is not number.");
            return parameter;
        }

        String generated = CharacterGenerator.createRandom(bound);
        console.writeLine(new Message(generated, "Generated: " + generated));
        parameter.getPasswordMap().put(parameter.getArgs().get(1), generated);

        if (parameter.getConfig().getAutosave()) {
            SaveUtil.save(parameter);
        }

        return parameter;
    }

}
