package shiranui.command;

import shiranui.utils.Crypt;
import shiranui.utils.config.Config;
import shiranui.utils.io.IoConsole;

import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * コマンドの実行に使用されるパラメーターを表すPOJO
 */
public class Parameter {

    /** パスワードマップ */
    private Map<String, String> passwordMap;
    /** コンソール */
    private IoConsole console;
    /** 引数 */
    private List<String> args;
    /** 設定 */
    private Config conf;
    /** 暗号化・復号化に使うCryptインスタンス */
    private Crypt crypt;

    /**
     * プライベートコンストラクタ―
     */
    private Parameter() {}

    /**
     * パラメーターオブジェクトを作成する。
     * @param passwordMap パスワードを保持するキャッシュ
     * @param console コンソール
     * @param crypt Crypt
     * @param args 引数
     * @param conf グローバル設定
     * @return 生成されたパラメーター
     */
    // TODO ビルダー提供
    public static Parameter create(
            Map<String, String> passwordMap,
            IoConsole console,
            Crypt crypt,
            List<String> args,
            Config conf) {
        Parameter parameter = new Parameter();
        parameter.passwordMap = passwordMap;
        parameter.console = console;
        parameter.crypt = crypt;
        parameter.args = args;
        parameter.conf = conf;
        parameter.check();
        return parameter;
    }

    private void check() {
        checkNotNull(passwordMap);
        checkNotNull(crypt);
        checkNotNull(args);
        checkNotNull(conf);
        checkNotNull(console);
    }

    public Map<String, String> getPasswordMap() {
        return passwordMap;
    }

    public IoConsole getConsole() {
        return console;
    }

    public List<String> getArgs() {
        return args;
    }

    public Config getConfig() {
        return conf;
    }

    public Crypt getCrypt() {
        return crypt;
    }

    public void setPasswordMap(Map<String, String> passwordMap) {
        this.passwordMap = passwordMap;
    }

    public void setConsole(IoConsole console) {
        this.console = console;
    }

    public void setArgs(List<String> args) {
        this.args = args;
    }

    public void setConfig(Config conf) {
        this.conf = conf;
    }

    public void setCrypt(Crypt crypt) {
        this.crypt = crypt;
    }

}
