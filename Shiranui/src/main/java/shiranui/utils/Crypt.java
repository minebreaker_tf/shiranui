package shiranui.utils;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 共通鍵を指定して暗号化・複合化を行うユーティリティークラス。<br />
 * 暗号化方式はAESで固定している。
 */
public final class Crypt {

    private static final String AES = "AES";
    private static final String CRYPT_ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final int IV_SIZE = 16;

    private final Key key;
    private final Cipher cipher;
    private final AlgorithmParameterSpec iv;

    /**
     * 指定した共通鍵を使用するインスタンスを作成する。
     * @param passcode 共通鍵
     */
    public Crypt(byte[] passcode) {
        checkNotNull(passcode);

        //パスワードをMD5ハッシュ化
        passcode = Hash.getInstance().getHash(passcode);
        //AESの鍵は128bitで、MD5は128bitを返す。
        this.key = new SecretKeySpec(passcode, AES);
        this.iv = new IvParameterSpec(Arrays.copyOf(passcode, IV_SIZE));
        Arrays.fill(passcode, (byte)0);
        try {
            this.cipher = Cipher.getInstance(CRYPT_ALGORITHM);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
            throw new Error(e);
        }
    }

    /**
     * 指定した共通鍵を使用するインスタンスを作成する。
     * <p>この実装は、{@code new Crypt(string.getBytes())}に等しい。
     * @param password 共通鍵
     */
    public Crypt(String password) {
        this(password.getBytes());
    }

    /**
     * 指定した共通鍵を使用するインスタンスを作成する。
     * @param password 共通鍵
     */
    public Crypt(char[] password) {
        this(String.valueOf(password).getBytes());
    }

    /**
     * このCryptインスタンスが保持する共通鍵で暗号化を行う。
     * @param src 暗号化するバイト配列
     * @return 暗号化されたバイト配列
     * @throws InvalidKeyException
     * @throws BadPaddingException
     */
    public byte[] encode(byte[] src) throws InvalidKeyException, BadPaddingException {
        checkNotNull(src);

        try {
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            return cipher.doFinal(src);
        } catch (InvalidAlgorithmParameterException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new Error(e);
        }
    }

    /**
     * このCryptインスタンスが保持する共通鍵で暗号化を行う。
     * @param src 暗号化する文字列
     * @return 暗号化されたバイト配列
     * @throws InvalidKeyException
     * @throws BadPaddingException
     */
    public byte[] encode(String src) throws InvalidKeyException, BadPaddingException {
        return encode(src.getBytes());
    }

    /**
     * このCryptインスタンスが保持する鍵で復号化を行う。
     * @param src 復号化するバイト配列
     * @return 復号化したバイト配列
     * @throws InvalidKeyException
     * @throws BadPaddingException
     */
    public byte[] decode(byte[] src) throws InvalidKeyException, BadPaddingException {
        checkNotNull(src);

        try {
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            return cipher.doFinal(src);
        } catch (InvalidAlgorithmParameterException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new Error(e);
        }
    }

}
