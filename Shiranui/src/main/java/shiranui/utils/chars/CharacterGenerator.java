package shiranui.utils.chars;

import java.util.EnumSet;
import java.util.Set;

import shiranui.utils.RandomUtil;

import static com.google.common.base.Preconditions.checkNotNull;
import static shiranui.utils.chars.Characters.CharSet;

/**
 * 暗号として適切な文字列を生成する。
 */
public class CharacterGenerator {

    /**
     * プライベートコンストラクタ―
     */
    private CharacterGenerator() {}

    /**
     * 指定された長さと文字セットのパスワードを生成する
     * @param length 生成する文字列の長さ
     * @param charSet 文字セット
     * @return 生成されたパスワード文字列
     */
    private static String createRandomString(int length, Set<Characters.CharSet> charSet) {
        checkNotNull(charSet);

        char[] chars = Characters.getChars(charSet);
        char[] generated = new char[length];

        for (int i = 0; i < length; i++) {
            int index = RandomUtil.getRandom(chars.length);
            generated[i] = chars[index];
        }

        return String.valueOf(generated);
    }

    /**
     * 指定された長さの大文字・小文字・数字を含む文字列を返す
     * @param length 生成するパスワードの長さ
     * @return 生成されたパスワード
     */
    public static String createRandom(int length) {
        return createRandomString(length, EnumSet.of(CharSet.LARGE_CAPITAL, CharSet.SMALL_CAPITAL, CharSet.NUMBER));
    }

}
