package shiranui.utils.chars;

import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 文字の集合を定義するクラス
 */
public class Characters {

    /**
     * 指定された文字集合の列挙体セットから、char配列を生成する。
     * @param charSet 生成したい文字集合のセット
     * @return 文字集合が表すcharを合成した配列
     */
    public static char[] getChars(Set<CharSet> charSet) {
        checkNotNull(charSet);

        char[] merged = new char[0];

        // TODO もっと賢い生成ロジックに
        for (CharSet set : charSet) {
            char[] c1 = merged;
            char[] c2 = CharSet.get(set);
            merged = new char[c1.length + c2.length];
            // TODO インスペクションに従いArrays.copyOFを使う
            for (int i = 0; i < c1.length; i++) {
                merged[i] = c1[i];
            }
            for (int i = 0; i < c2.length; i++) {
                merged[i + c1.length] = c2[i];
            }
        }

        return merged;
    }

    /**
     * 文字の種類を表す集合である列挙体
     */
    public static enum CharSet {
        SMALL_CAPITAL, LARGE_CAPITAL, NUMBER, SYMBOLS;

        public static char[] get(CharSet set) {
            switch (set) {
            case SMALL_CAPITAL:
                return Characters.smallCapital;
            case LARGE_CAPITAL:
                return Characters.largeCapital;
            case NUMBER:
                return Characters.number;
            case SYMBOLS:
                return Characters.symbols;
            default:
                throw new NoSuchFieldError();
            }
        }
    }

    /**
     * 小文字
     */
    public static char[] smallCapital = new char[] {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z'
    };

    /**
     * 大文字
     */
    public static char[] largeCapital = new char[] {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z'
    };

    /**
     * 数字
     */
    public static char[] number = new char[] {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };

    /**
     * 記号
     */
    public static char[] symbols = new char[] {
            '!', '?', '-', '_', '*'
    };

}
