package shiranui.utils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * クリップボードを扱うユーティリティークラス
 */
public final class ClipboardUtil {

    /** プライベートコンストラクター */
    private ClipboardUtil() {}

    /**
     * クリップボードに文字列をコピーする。
     * @param value コピーする文字列
     */
    public static void setString(String value) {
        checkNotNull(value);

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();

        clipboard.setContents(new StringSelection(value), null);
    }

}
