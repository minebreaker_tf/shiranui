package shiranui.utils.io;

import javax.annotation.Nullable;
import java.io.Console;

/**
 * System.console()をIoConsoleインターフェースでラップしている。
 */
final class JavaConsoleWrapper implements IoConsole {


    /** ラップしているConsoleオブジェクト */
    private Console console;
    /** 設定された出力レベル */
    private final Message.Level level;

    /**
     * プライベートコンストラクタ
     * @param level 出力レベル
     */
    private JavaConsoleWrapper(Message.Level level) {
        this.level = level;
    }

    /**
     * このクラスのインスタンスを取得するための静的ファクトリーメソッド。<br />
     * @param level 出力レベル
     * @return このクラスのインスタンス。コンソールの取得に失敗した場合、null
     */
    public static JavaConsoleWrapper getInstance(Message.Level level) {
        Console console = System.console();

        if (console == null) {
            return null;
        } else {
            JavaConsoleWrapper instance = new JavaConsoleWrapper(level);
            instance.console = System.console();
            return instance;
        }
    }

    @Override
    public String readLine() {
        return console.readLine();
    }

    @Override
    public char[] readPassword() {
        return console.readPassword();
    }

    @Override
    public void write(@Nullable Message message) {
        if (message == null) {
            message = new Message("");
        }

        console.writer().write(message.get(level));
        console.writer().flush();
    }

}
