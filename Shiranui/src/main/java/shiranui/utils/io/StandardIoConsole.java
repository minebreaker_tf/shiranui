package shiranui.utils.io;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * 標準入出力を使用するIoConsole実装
 */
public final class StandardIoConsole implements IoConsole {

    private final BufferedReader reader;
    private final BufferedWriter writer;
    private final Message.Level level;

    /**
     * コンストラクター
     * @param level 出力レベル
     */
    public StandardIoConsole(Message.Level level) {
        // 標準入出力をバッファ付きでラップする
        this.reader = new BufferedReader(new InputStreamReader(System.in));
        this.writer = new BufferedWriter(new OutputStreamWriter(System.out));

        this.level = level;
    }

    @Override
    public String readLine() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public char[] readPassword() {
        String line = readLine();
        if (line == null) {
            return new char[0];
        } else {
            return line.toCharArray();
        }
    }

    @Override
    public void write(@Nullable Message message) {
        if (message == null) {
            message = new Message("");
        }

        try {
            writer.write(message.get(level));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
