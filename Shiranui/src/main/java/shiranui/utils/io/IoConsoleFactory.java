package shiranui.utils.io;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * IoConsoleを作成するファクトリークラス。<br />
 * 各実装クラスのインスタンス化が禁止されているわけではない。
 */
public final class IoConsoleFactory {

    /**
     * プライベートコンストラクター
     */
    private IoConsoleFactory() {}

    /**
     * 適切に利用できるIoConsoleクラスを返す。
     * @param level 設定したいログレベル
     * @return JavaConsoleWrapperが利用できる場合、そのインスタンス。そうでない場合、StandardIoConsoleインスタンス
     */
    public static IoConsole getConsole(Message.Level level) {
        checkNotNull(level);

        IoConsole console = JavaConsoleWrapper.getInstance(level);
        return console != null ? console : new StandardIoConsole(level);
    }

}
