package shiranui.utils.io;

/**
 * 標準入出力の扱いをラップするインターフェース。<br />
 * System.console()がnullを返したときのために作成。
 */
public interface IoConsole {

    /**
     * 文字列を一行読み取る。
     * @return 文字列
     */
    public abstract String readLine();

    /**
     * パスワードを読み取る。
     * @return パスワード
     */
    public abstract char[] readPassword();

    /**
     * 「Y」か「N」いずれかの入力が得られるまで入力を求める。<br />
     * @return Yの場合true、Nの場合false
     */
    public default boolean readYN() {
        while (true) {
            String value = readLine();
            String regularized = value.toUpperCase();

            if (regularized.equals("Y")) {
                return true;
            } else if (regularized.equals("N")) {
                return false;
            } else {
                writeLine(new Message("Enter Y or N."));
            }
        }
    }

    /**
     * メッセージを出力する。<br />
     * 出力レベルの制御は各コンソールの実装に依存する。
     * @param message 出力するメッセージ
     */
    public abstract void write(Message message);

    /**
     * 終端に改行コードを追加したメッセージを出力する。<br />
     * 出力レベルの制御は各コンソールの実装に依存する。
     * @param message 出力するメッセージ
     */
    public default void writeLine(Message message) {
        write(new Message(message.getSimple() + System.lineSeparator(), message.getFull() + System.lineSeparator()));
    }

    /**
     * 文字列を出力する。
     * @param message 出力する文字列
     */
    public default void write(String message) {
        write(new Message(message));
    }

    /**
     * 終端に改行コードを追加した文字列を出力する。
     * @param message 出力する文字列
     */
    public default void writeLine(String message) {
        writeLine(new Message(message));
    }

}
