package shiranui.utils.io;

import javax.annotation.Nullable;

/**
 * 複数の表現形式を持つ、ひとつの出力メッセージを表すクラス。<br />
 */
public final class Message {

    private final String simple;
    private final String full;

    /**
     * コンストラクター
     * @param simple 単純な形式のメッセージ
     * @param full 長い形式のメッセージ
     */
    public Message(@Nullable String simple, @Nullable String full) {
        this.simple = simple != null ? simple : "";
        this.full = full != null ? full : "";
    }

    /**
     * 短い形式を指定しないコンストラクター
     * @param full 長い形式のメッセージ
     */
    public Message(String full) {
        this("", full);
    }

    /**
     * 短い形式のメッセージを返す。
     * @return メッセージ
     */
    public String getSimple() {
        return simple;
    }

    /**
     * 長い形式のメッセージを返す。
     * @return メッセージ
     */
    public String getFull() {
        return full;
    }

    /**
     * 指定されたレベルのメッセージを返す。
     * @param level 出力したいレベル
     * @return 当該レベルのメッセージ
     */
    public String get(Level level) {
        return level.get(this);
    }

    /**
     * メッセージのレベルを示す列挙体
     */
    public static enum Level {
        /**
         * 単純な形式のメッセージのレベル
         */
        SIMPLE {
            @Override
            String get(Message message) {
                return message.simple;
            }
        },
        /**
         * 長い形式のメッセージのレベル
         */
        FULL {
            @Override
            String get(Message message) {
                return message.full;
            }
        },
        ;

        /**
         * 自身のレベルに対応するメッセージを返す。<br />
         * @param message 判定を行いたいメッセージ
         * @return 自身のレベルに対応したメッセージ
         */
        abstract String get(Message message);
    }

}
