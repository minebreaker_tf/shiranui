package shiranui.utils.config;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

// グローバルなコンフィグってBeanスタイルの方がいいのだろうか?
/**
 * 設定ファイルの数値を保持するPOJO。<br />
 * 直接のインスタンス化は原則避け、ConfigBuilderクラスを使用する。
 */
public final class Config {

    /** 自動保存 */
    private final boolean autosave;
    /** パスワード。設定されていると自動ログインする */
    private final String password;
    /** データファイルのパス */
    private final String data;

    public Config(boolean autosave, String password, String data) {
        checkArgument(password != null, "password is null");
        checkArgument(!isNullOrEmpty(data), "data is null or empty");

        this.autosave = autosave;
        this.password = password;
        this.data = data;
    }

    public boolean getAutosave() {
        return autosave;
    }

    public String getPassword() {
        return password;
    }

    public String getData() {
        return data;
    }

}
