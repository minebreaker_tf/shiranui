package shiranui.utils.config;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;

import com.google.gson.Gson;

import shiranui.consts.Consts;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Configクラスのインスタンスを生成する。<br />
 * ここで生成されたインスタンスは、全てのパラメータが設定されている状態になる。
 */
public final class ConfigBuilder {

    private boolean autosave;
    private String password;
    private String data;

    /**
     * コンストラクター
     */
    public ConfigBuilder() {
        this.autosave = false;
        this.password = "";
        this.data = Consts.DATA_FILE;
    }

    /**
     * Configインスタンスを生成する。
     * @return パラメーターが設定された適切なconfigインスタンス
     * @throws IllegalStateException 設定されたパラメーターが不正だった場合
     */
    public Config build() {
        if (!checkIfValid()) throw new IllegalStateException();

        return new Config(
                autosave,
                password,
                data);
    }

    public boolean checkIfValid() {
        if (password == null) return false;
        if (isNullOrEmpty(data)) return false;

        return true;
    }

    /**
     * オートセーブの有無を設定する
     * @param autosave オートセーブを有効にする場合true
     * @return this
     */
    public ConfigBuilder autosave(boolean autosave) {
        this.autosave = autosave;
        return this;
    }

    /**
     * データファイルのパスを設定する。
     * @param data データファイルのパスのString表現
     * @return this
     */
    public ConfigBuilder data(String data) {
        this.data = data;
        return this;
    }

    /**
     * オートログイン用パスワードを設定する。
     * @param password 設定したいパスワード
     * @return this
     */
    public ConfigBuilder password(String password) {
        this.password = password;
        return this;
    }

    /**
     * Json設定ファイルを読み込む。<br />
     * @param jsonPath JSON設定ファイルのパス
     * @return this
     * @throws IOException 設定ファイルの読み取り時に例外が発生したとき
     */
    public static Config readFromJson(Path jsonPath) throws IOException {
        checkNotNull(jsonPath);

        Reader reader = Files.newBufferedReader(jsonPath);
        return new Gson().fromJson(reader, Config.class);
    }

}
