package shiranui.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * MD5ハッシュを作成するためのユーティリティークラス
 */
public final class Hash {

    private static final String ALGORITHM = "MD5";

    /** シングルトンインスタンス */
    private static final Hash singleton = new Hash();

    /**
     * ハッシュを作成するメッセージダイジェスト
     */
    private final MessageDigest messageDigest;

    /** プライベートコンストラクタ― */
    private Hash() {
        try {
            this.messageDigest = MessageDigest.getInstance(ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            // アルゴリズムは固定値なので、基本的にエラーは起こらない
            e.printStackTrace();
            throw new Error(e);
        }
    }

    /** シングルトンインスタンスを取得する。 */
    public static Hash getInstance() {
        return singleton;
    }

    /**
     * バイト配列のハッシュをバイト配列で取得する。
     * @param target ハッシュを取得するバイト配列
     * @return 生成されたハッシュ
     */
    public byte[] getHash(byte[] target) {
        checkNotNull(target);

        messageDigest.update(target);
        return messageDigest.digest();
    }

    /**
     * バイト配列のハッシュを文字列で取得する。
     * @param target ハッシュを取得するバイト配列
     * @return 生成されたハッシュの16進数表現
     */
    public String getHashString(byte[] target) {
        return DatatypeConverter.printHexBinary(getHash(target));
    }

}
