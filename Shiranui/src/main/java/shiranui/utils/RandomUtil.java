package shiranui.utils;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

/**
 * 乱数を取得するためのコンビニエンスクラス
 */
public final class RandomUtil {

    private static final String ALGORITHM = "SHA1PRNG";
    private static final String PROVIDER = "SUN";

    /** 乱数発生器 */
    private static final SecureRandom generator;

    static {
        try {
            generator = SecureRandom.getInstance(ALGORITHM, PROVIDER);
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            // アルゴリズムとプロバイダーは決め打ちなのでエラーになることはないはず。
            e.printStackTrace();
            throw new Error();
        }
    }

    /** プライベートコンストラクター */
    private RandomUtil() {}

    /**
     * 0からboundまで(boundは含まない)の乱数を取得する。
     * @param bound 境界値
     * @return 生成された乱数
     */
    public static int getRandom(int bound) {
        return generator.nextInt(bound);
    }

}
