package shiranui.utils.file;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;

import javax.crypto.BadPaddingException;

import org.apache.commons.io.FileUtils;

import shiranui.command.Parameter;
import shiranui.utils.ObjectConverter;
import shiranui.utils.io.IoConsole;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * ファイルの保存処理に関するユーティリティークラス
 */
public final class SaveUtil {

    /**
     * プライベートコンストラクタ―
     */
    private SaveUtil() {}

    /**
     * パスワードのマップをシリアライズして暗号化し、ファイルに保存する。
     * @param parameter 各情報を持ったパラメーター
     */
    public static void save(Parameter parameter) {
        checkNotNull(parameter);

        byte[] serialized = ObjectConverter.serialize(parameter.getPasswordMap());
        byte[] crypted;
        try {
            crypted = parameter.getCrypt().encode(serialized);
        } catch (InvalidKeyException | BadPaddingException e) {
            //暗号化なので基本的にエラーは出ないはず
            e.printStackTrace();
            throw new Error(e);
        }

        saveFile(parameter.getConsole(), Paths.get(parameter.getConfig().getData()), crypted);
    }

    /**
     * 指定されたパスにバイト配列を保存する。<br />
     * 例外処理はコンソールに出力するのみとする。
     * @param console 通知用コンソール
     * @param path 保存先
     * @param content 保存するバイト配列
     */
    private static void saveFile(IoConsole console, Path path, byte[] content) {
        checkNotNull(console);
        checkNotNull(content);

        try {
            FileUtils.writeByteArrayToFile(path.toFile(), content);
            console.writeLine("Successfully saved.");
        } catch (IOException e) {
            console.writeLine("Failed to write");
            e.printStackTrace();
        }
    }

}
