package shiranui.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import shiranui.utils.io.IoConsole;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * サービスのヘルパークラス。<br />
 * サービス共通で使いそうな静的メソッドを提供。
 */
public final class ServiceUtil {

    /**
     * プライベートコンストラクタ
     * <p>インスタンス化を禁止する。
     */
    private ServiceUtil() {}

    /**
     * ファイルをコンソールへ読み込む。<br />
     * 存在しなかった場合、親ディレクトリと空のファイルを作成する。<br />
     * @param console 使用するコンソール
     * @param file 読み取り対象のファイル
     * @return 読み取ったファイルのバイト配列
     */
    public static byte[] readCryptedContent(IoConsole console, Path file) {
        checkNotNull(console);
        checkNotNull(file);

        try {
            if (!Files.exists(file)) {
                console.writeLine("The data file is not found. Tries to create.");
                Path parent = file.getParent();
                if (parent != null && !Files.exists(parent)) {
                    Files.createDirectories(parent);
                }
                Files.createFile(file);
            }

            // Files.readAllBytes()でいいんじゃ……
            return FileUtils.readFileToByteArray(file.toFile());

        } catch (IOException e) {
            e.printStackTrace();
            throw new Error(e);
        }
    }

    /**
     * バイト配列をマップのインスタンスに変換する。<br />
     * 失敗した場合、エラーを吐く。
     * @param console 対話に使用するコンソール
     * @param content 変換対象のバイト配列
     * @return デシリアライズされたマップ
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> convertBytesToMap(IoConsole console, byte[] content) {
        try {
            return (Map<String, String>)ObjectConverter.deserialize(content);
        } catch (ClassCastException e) {
            console.writeLine("Password file is broken.");
            throw new Error(e);
            //TODO エラーの代わりに、処理を継続できるようにする
        }
    }

}
