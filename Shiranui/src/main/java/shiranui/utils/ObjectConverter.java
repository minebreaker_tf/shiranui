package shiranui.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * オブジェクトのシリアライズ・デシリアライズを行うユーティリティークラス。
 */
public final class ObjectConverter {

    /** プライベートコンストラクタ― */
    private ObjectConverter() {}

    /**
     * オブジェクトをバイト配列にシリアライズする。
     * @param content 変換対象
     * @return シリアライズされたバイト配列。
     */
    public static byte[] serialize(Object content) {
        checkNotNull(content);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(content);
            oos.close();
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Error();
        }
    }

    /**
     * バイト配列をオブジェクトにデシリアライズする。
     * @param content 変換対象のバイト配列
     * @return 変換されたオブジェクト
     */
    public static Object deserialize(byte[] content) {
        checkNotNull(content);

        ByteArrayInputStream bis = new ByteArrayInputStream(content);
        try (ObjectInputStream ois = new ObjectInputStream(bis)) {
            return ois.readObject();
            // 普通なら呼び出し元がキャッチするべきだが、Mapのシリアライズにしか使われないので。
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new Error(e);
        }
    }

}
