package shiranui;

import java.io.IOException;
import java.nio.file.Paths;

import shiranui.consts.Consts;
import shiranui.utils.config.Config;
import shiranui.utils.config.ConfigBuilder;

/**
 * メインクラス。静的mainメソッドを持ち、JVM起動時に呼ばれる。
 */
public final class Main {

    /**
     * プライベートコンストラクタ<br />
     * インスタンス化を禁止する
     */
    private Main() {}

    /**
     * 静的mainメソッド。JVMにより起動される。
     * @param args JVM引数
     */
    public static void main(String args[]) {

        Service service;
        if (args.length == 0) {
            service = ServiceInteractive.getInstance();
        } else {
            service = ServiceNonInteractive.getInstance();
        }

        Config config;
        try {
            config = ConfigBuilder.readFromJson(Paths.get(Consts.CONFIG_FILE));
        } catch (IOException e) {
            System.err.println("Failed to read config file. Use default.");
            e.printStackTrace();
            config = new ConfigBuilder()
                    .autosave(false)
                    .data(Consts.DATA_FILE)
                    .password("").build();
        }

        service.execute(args, config);

    }
}
