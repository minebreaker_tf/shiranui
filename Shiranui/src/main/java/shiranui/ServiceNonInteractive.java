package shiranui;

import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;

import shiranui.command.Command;
import shiranui.command.Parameter;
import shiranui.utils.Crypt;
import shiranui.utils.ServiceUtil;
import shiranui.utils.config.Config;
import shiranui.utils.io.IoConsole;
import shiranui.utils.io.IoConsoleFactory;
import shiranui.utils.io.Message;

/**
 * 非対話的サービスを提供する。<br />
 * バッチ処理での利用を想定。
 */
final class ServiceNonInteractive implements Service {

    /** シングルトンインスタンス */
    private static final ServiceNonInteractive singleton = new ServiceNonInteractive();

    /** インスタンス化禁止 */
    private ServiceNonInteractive() {}

    /**
     * シングルトンインスタンスを取得する。
     * @return インスタンス
     */
    public static ServiceNonInteractive getInstance() {
        return singleton;
    }

    @Override
    public void execute(String[] original, Config conf) {

        if (original.length <= 0) {
            throw new Error();
        }

        IoConsole console = IoConsoleFactory.getConsole(Message.Level.SIMPLE);

        byte[] contentCrypted = ServiceUtil.readCryptedContent(console, Paths.get(conf.getData()));

        Crypt crypt;
        List<String> args;

        if (conf.getPassword() != null && !conf.getPassword().isEmpty()) { //パスワードが設定されていた場合
            crypt = new Crypt(conf.getPassword());
            args = Arrays.asList(original);
        } else { // 第一引数をパスワードとみなす
            crypt = new Crypt(original[0]);
            args = createArgs(original);
        }

        Map<String, String> dataMap;

        try {
            byte[] contentDecrypted = crypt.decode(contentCrypted);
            //解読に成功
            dataMap = ServiceUtil.convertBytesToMap(console, contentDecrypted);
        } catch (InvalidKeyException | BadPaddingException e) {
            //デコードに関連した例外が発生
            console.writeLine("Could not decode data file.");
            return;
        }

        Parameter parameter = Parameter.create(dataMap, console, crypt, args, conf);
        Command command = Command.getCommand(args.get(0));
        command.execute(parameter);
    }

    private List<String> createArgs(String[] original) {
        String[] newArgs = Arrays.copyOfRange(original, 1, original.length);
        return Arrays.asList(newArgs);
    }

}
