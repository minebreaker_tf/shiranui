package shiranui.consts;

/**
 * 定数クラス。汎用的に使われる定数を保持する。
 */
public final class Consts {

    /**
     * プライベートコンストラクタ―
     */
    private Consts() {}

    /** アプリ名 */
    public static final String SHIRANUI = "Shiranui";
    /** バージョン */
    public static final String VERSION = "0.2.0 (Beta)";
    /** 起動時バナー */
    public static final String BANNER = "- Simple Password Manager";
    /** アプリの説明. SHIRANUI + VERSION + BANNER */
    public static final String EXPLAIN = String.join(" ", SHIRANUI, VERSION, BANNER);

    /** デフォルトのデータファイルのロケーション */
    public static final String DATA_FILE = "dat/data.bin";
    /** デフォルトのコンフィグファイルのロケーション */
    public static final String CONFIG_FILE = "conf/conf.json";

}
