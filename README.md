# Shiranui - Java-based CLI password manager -

**Shiranui** is CLI password manager written in Java, witch aims to be Simple, Compact, Easy-to-use.

## Quickstart

1. [Download](https://bitbucket.org/mb_terraflops/shiranui/downloads/shiranui_0.2.0.zip)
2. Unzip
3. Add environmental variable

## Instalation

### 1. Download shiranui

Latest:
[Shiranui 0.2.0 (Beta)](https://bitbucket.org/mb_terraflops/shiranui/downloads/shiranui_0.2.0.zip)

Old:
[Shiranui 0.1.0 (Alpha)](https://bitbucket.org/mb_terraflops/shiranui/downloads/shiranui_0.1.0.zip)

### 2. Unzip

Unzip the downloaded file to any directory you want.

### 3. Add environmental variable

Add 'shianui' directory (where contains 'shiranui.bat') to your environmental variables.

## How to use

Execute `shiranui` on terminal to run.
```
#!dos
> shiranui
Shiranui 0.1.0 (Alpha) - Simple Password Manager
Data File MD5: PASSWORD_MD5_IS_SHOWN_HERE
Please specify your password: ENTER_YOUR_MASTER_PASSWORD_HERE
>
```
Then, use `help` command to find commands Shiranui can do.

## Isn't it safe to use?
Saving password is always dangerous, even if crypted.

## License
Sleepycat